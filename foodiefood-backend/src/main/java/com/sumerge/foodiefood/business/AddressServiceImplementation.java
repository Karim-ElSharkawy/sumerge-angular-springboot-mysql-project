package com.sumerge.foodiefood.business;

import com.sumerge.foodiefood.model.Address;
import com.sumerge.foodiefood.repositories.AddressRepository;
import com.sumerge.foodiefood.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImplementation implements AddressService {

    @Autowired
    private AddressRepository addressRepository;


    public List<Address> listAllByUserId(Integer userId) {
        return addressRepository.findAllByUserId(userId);
    }


    public Address getById(Integer id) {
        return addressRepository.findById(id).get();
    }


    public void saveOrUpdate(Address domainObject) {
        addressRepository.save(domainObject);
    }


    public void delete(Integer id) {
        addressRepository.deleteById(id);
    }
}
