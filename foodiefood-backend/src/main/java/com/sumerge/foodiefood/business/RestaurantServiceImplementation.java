package com.sumerge.foodiefood.business;

import com.sumerge.foodiefood.model.Restaurant;
import com.sumerge.foodiefood.repositories.RestaurantRepository;
import com.sumerge.foodiefood.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RestaurantServiceImplementation implements RestaurantService {
    @Autowired
    private RestaurantRepository restaurantRepository;

    public List<Restaurant> listAll() {
        List<Restaurant> outputList = new ArrayList<>();
        restaurantRepository.findAll().forEach(outputList::add);
        return outputList;
    }

    public Restaurant getById(Integer id) {
        return restaurantRepository.findById(id).get();
    }

    public void saveOrUpdate(Restaurant domainObject) {
        restaurantRepository.save(domainObject);
    }

    public void delete(Integer id) {
        restaurantRepository.deleteById(id);
    }
}
