package com.sumerge.foodiefood.business;

import com.sumerge.foodiefood.model.Item;
import com.sumerge.foodiefood.repositories.ItemRepository;
import com.sumerge.foodiefood.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImplementation implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    public Object listAllByRestaurantId(Integer restaurantId) {
        return itemRepository.findByRestaurantId(restaurantId);
    }

    public Item getById(Integer id) {
        return itemRepository.findById(id).get();
    }

    public void saveOrUpdate(Item domainObject) {
        itemRepository.save(domainObject);
    }

    public void delete(Integer id) {
        itemRepository.deleteById(id);
    }
}
