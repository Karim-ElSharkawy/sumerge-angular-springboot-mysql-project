package com.sumerge.foodiefood.business;

import com.sumerge.foodiefood.model.Order;
import com.sumerge.foodiefood.repositories.OrderRepository;
import com.sumerge.foodiefood.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImplementation implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public List<Order> listAllByUserId(Integer userId) {
        return orderRepository.findByUserId(userId);
    }

    public Order getById(Integer id) {
        return orderRepository.findById(id).get();
    }

    public void saveOrUpdate(Order domainObject) {
        orderRepository.save(domainObject);
    }

    public void delete(Integer id) {
        orderRepository.deleteById(id);
    }
}
