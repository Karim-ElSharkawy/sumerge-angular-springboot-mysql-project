package com.sumerge.foodiefood.business;


import com.sumerge.foodiefood.model.OrderItem;
import com.sumerge.foodiefood.repositories.OrderItemRepository;
import com.sumerge.foodiefood.services.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderItemServiceImplementation implements OrderItemService {
    @Autowired
    private OrderItemRepository orderItemRepository;

    @Override
    public List<OrderItem> listAllByOrderId(Integer orderId) {
        return orderItemRepository.findByOrderId(orderId);
    }

    @Override
    public List<OrderItem> listAllByItemId(Integer itemId) {
        return orderItemRepository.findByOrderId(itemId);
    }

    public OrderItem getById(Integer id) {
        return orderItemRepository.findById(id).get();
    }

    public void saveOrUpdate(OrderItem domainObject) {
        orderItemRepository.save(domainObject);
    }

    public void delete(Integer id) {
        orderItemRepository.deleteById(id);
    }
}
