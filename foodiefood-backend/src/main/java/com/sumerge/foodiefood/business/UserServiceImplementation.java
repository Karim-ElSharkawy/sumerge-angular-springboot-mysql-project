package com.sumerge.foodiefood.business;

import com.sumerge.foodiefood.model.User;
import com.sumerge.foodiefood.repositories.AddressRepository;
import com.sumerge.foodiefood.repositories.UserRepository;
import com.sumerge.foodiefood.services.UserService;
import com.sumerge.foodiefood.util.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImplementation implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Override
    public Integer findUserExistsByName(String username) {
        List<User> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);

        userList = userList.stream().filter((user) -> user.getUsername().equals(username)).collect(Collectors.toList());

        if(userList.size() > 0) {
            System.out.println("ID: " + userList.get(0).getId());
            return userList.get(0).getId();
        }
        return -1;
    }

    public Integer loginUserByCred(String username, String password){

        List<User> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        userList = userList.stream().filter((user) -> user.getUsername().equals(username)
                && user.getPassword()
                .equals(new SecurityConfig().passwordEncoder().encode(password))).collect(Collectors.toList());

        if(userList.size() > 0) {
            System.out.println("ID: " + userList.get(0).getUsername());
            return userList.get(0).getId();
        }
        return -1;
    }
    public List<User> listAll() {
        List<User> outputList = new ArrayList<>();
        userRepository.findAll().forEach(outputList::add);
        return outputList;
    }

    public User getById(Integer userId) {
        return userRepository.findById(userId).get();
    }

    public void saveOrUpdate(User domainObject) {
        userRepository.save(domainObject);
    }

    @Transactional
    public void delete(Integer userId) {
        if(addressRepository.findAllByUserId(userId).size() > 0)
            addressRepository.deleteByUserId(userId);

        userRepository.deleteById(userId);
    }

}
