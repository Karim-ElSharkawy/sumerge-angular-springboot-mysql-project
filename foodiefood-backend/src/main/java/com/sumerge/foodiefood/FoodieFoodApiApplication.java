package com.sumerge.foodiefood;

import com.sumerge.foodiefood.repositories.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class FoodieFoodApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodieFoodApiApplication.class, args);
    }

}
