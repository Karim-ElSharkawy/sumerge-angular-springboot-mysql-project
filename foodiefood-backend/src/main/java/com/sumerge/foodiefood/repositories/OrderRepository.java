package com.sumerge.foodiefood.repositories;

import com.sumerge.foodiefood.model.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Integer> {
    List<Order> findByUserId(Integer userId);
}
