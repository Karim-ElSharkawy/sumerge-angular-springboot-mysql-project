package com.sumerge.foodiefood.repositories;

import com.sumerge.foodiefood.model.OrderItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderItemRepository extends CrudRepository<OrderItem, Integer> {
    List<OrderItem> findByOrderId(Integer orderId);

    List<OrderItem> findByItemId(Integer itemId);
}
