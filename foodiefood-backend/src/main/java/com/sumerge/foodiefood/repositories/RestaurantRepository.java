package com.sumerge.foodiefood.repositories;

import com.sumerge.foodiefood.model.Restaurant;
import org.springframework.data.repository.CrudRepository;

public interface RestaurantRepository extends CrudRepository<Restaurant, Integer> {

}
