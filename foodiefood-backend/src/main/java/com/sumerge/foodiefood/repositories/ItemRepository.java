package com.sumerge.foodiefood.repositories;

import com.sumerge.foodiefood.model.Item;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemRepository extends CrudRepository<Item, Integer> {
    List<Item> findByRestaurantId(Integer restaurantId);
}
