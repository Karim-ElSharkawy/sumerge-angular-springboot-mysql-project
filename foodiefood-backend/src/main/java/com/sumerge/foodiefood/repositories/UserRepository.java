package com.sumerge.foodiefood.repositories;

import com.sumerge.foodiefood.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String username);
}
