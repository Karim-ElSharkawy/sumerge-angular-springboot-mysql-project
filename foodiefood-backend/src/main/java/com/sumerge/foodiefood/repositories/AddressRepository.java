package com.sumerge.foodiefood.repositories;

import com.sumerge.foodiefood.model.Address;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AddressRepository extends CrudRepository<Address, Integer> {

    List<Address> findAllByUserId(Integer userId);

    List<Address> deleteByUserId(Integer userId);
}
