package com.sumerge.foodiefood.services;

import com.sumerge.foodiefood.model.Order;

import java.util.List;

public interface OrderService {
    List<Order> listAllByUserId(Integer userId);

    Order getById(Integer id);

    void saveOrUpdate(Order domainObject);

    void delete(Integer id);
}
