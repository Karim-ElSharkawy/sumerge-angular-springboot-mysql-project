package com.sumerge.foodiefood.services;

import com.sumerge.foodiefood.model.User;

import java.util.List;

public interface UserService {
    Integer findUserExistsByName(String username);

    Integer loginUserByCred(String username, String password);

    List<User> listAll();

    User getById(Integer userId);

    void saveOrUpdate(User domainObject);

    void delete(Integer userId);
}
