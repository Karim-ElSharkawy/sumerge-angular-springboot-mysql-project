package com.sumerge.foodiefood.services;

import com.sumerge.foodiefood.model.Address;

import java.util.List;

public interface AddressService {
    List<Address> listAllByUserId(Integer userId);

    Address getById(Integer id);

    void saveOrUpdate(Address domainObject);

    void delete(Integer id);
}
