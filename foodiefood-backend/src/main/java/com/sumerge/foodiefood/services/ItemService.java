package com.sumerge.foodiefood.services;

import com.sumerge.foodiefood.model.Item;

import java.util.List;

public interface ItemService {
    Object listAllByRestaurantId(Integer restaurantId);

    Item getById(Integer id);

    void saveOrUpdate(Item domainObject);

    void delete(Integer id);
}
