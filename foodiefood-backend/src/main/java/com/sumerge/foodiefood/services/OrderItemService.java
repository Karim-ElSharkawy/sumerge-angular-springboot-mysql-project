package com.sumerge.foodiefood.services;

import com.sumerge.foodiefood.model.OrderItem;

import java.util.List;

public interface OrderItemService {
    List<OrderItem> listAllByOrderId(Integer orderId);

    List<OrderItem> listAllByItemId(Integer itemId);

    OrderItem getById(Integer orderItemId);

    void saveOrUpdate(OrderItem domainObject);

    void delete(Integer id);
}
