package com.sumerge.foodiefood.services;

import com.sumerge.foodiefood.model.Restaurant;

import java.util.List;

public interface RestaurantService {
    List<Restaurant> listAll();

    Restaurant getById(Integer id);

    void saveOrUpdate(Restaurant domainObject);

    void delete(Integer id);
}
