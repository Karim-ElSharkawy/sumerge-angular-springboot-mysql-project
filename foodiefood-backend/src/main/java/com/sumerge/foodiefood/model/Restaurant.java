package com.sumerge.foodiefood.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "restaurants")
public class Restaurant implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String phone;
    private String type;
    private String img;
    private String description;
    private float rating;

    public Restaurant() {
    }

    public Restaurant(String name, String phone, String type, String description, float rating) {
        this.name = name;
        this.phone = phone;
        this.type = type;
        this.description = description;
        this.rating = rating;
    }

    public Restaurant(String name, String phone, String type, String img, String description, float rating) {
        this.name = name;
        this.phone = phone;
        this.type = type;
        this.img = img;
        this.description = description;
        this.rating = rating;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRating() {
        return rating;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
