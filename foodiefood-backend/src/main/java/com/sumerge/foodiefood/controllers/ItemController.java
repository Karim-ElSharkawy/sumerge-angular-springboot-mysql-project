package com.sumerge.foodiefood.controllers;

import com.sumerge.foodiefood.model.Item;
import com.sumerge.foodiefood.model.Restaurant;
import com.sumerge.foodiefood.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/restaurants/{restaurantId}/items")
public class ItemController {

    private ItemService itemService;

    @Autowired
    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    // --- GET OPERATIONS ---
    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public Object listItems(@PathVariable Integer restaurantId) {
        return itemService.listAllByRestaurantId(restaurantId);
    }

    @RequestMapping(value = "/{itemId}", method = RequestMethod.GET)
    public Item getItem(@PathVariable Integer restaurantId, @PathVariable Integer itemId) {
        return itemService.getById(itemId);
    }

    // --- POST/PUT OPERATIONS ---
    @RequestMapping(value = {"/", "", "/{itemId}"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public void addOrUpdateItem(@RequestBody Item item, @PathVariable Integer restaurantId) {
        Restaurant restaurant = new Restaurant();
        restaurant.setId(restaurantId);
        item.setRestaurant(restaurant);
        itemService.saveOrUpdate(item);
    }

    // --- DELETE OPERATIONS ---
    @RequestMapping(value = "/{itemId}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer restaurantId, @PathVariable Integer itemId) {
        itemService.delete(itemId);
    }

}
