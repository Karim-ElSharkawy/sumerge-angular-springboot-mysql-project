package com.sumerge.foodiefood.controllers;

import com.sumerge.foodiefood.model.Address;
import com.sumerge.foodiefood.model.User;
import com.sumerge.foodiefood.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users/{userId}")
public class AddressController {

    private AddressService addressService;

    @Autowired
    public void setUserService(AddressService addressService) {
        this.addressService = addressService;
    }

    @RequestMapping(value = "/addresses", method = RequestMethod.GET)
    public List<Address> getAddressesByUsername(@PathVariable Integer userId) {
        return this.addressService.listAllByUserId(userId);

    }

    @RequestMapping(value = "/address/{addressId}", method = RequestMethod.GET)
    public Address getAddressByUsername(@PathVariable Integer userId, @PathVariable Integer addressId) {
        return addressService.getById(addressId);
    }

    // --- POST/PUT OPERATIONS ---
    @RequestMapping(value = "/address", method = {RequestMethod.POST, RequestMethod.PUT})
    public void addOrUpdateAddress(@RequestBody Address address, @PathVariable Integer userId) {
        User user = new User();
        user.setId(userId);
        address.setUser(user);
        addressService.saveOrUpdate(address);
    }

    // --- DELETE OPERATIONS ---
    @RequestMapping(value = "/address/{addressId}", method = RequestMethod.DELETE)
    public void deleteAddress(@PathVariable Integer userId, @PathVariable Integer addressId) {
        addressService.delete(addressId);
    }
}
