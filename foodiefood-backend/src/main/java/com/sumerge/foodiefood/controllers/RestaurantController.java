package com.sumerge.foodiefood.controllers;

import com.sumerge.foodiefood.model.Restaurant;
import com.sumerge.foodiefood.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/restaurants")
public class RestaurantController {
    private RestaurantService restaurantService;

    @Autowired
    public void setRestaurantService(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public List<Restaurant> listRestaurants() {
        return restaurantService.listAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public List<Restaurant> listRestaurantsByPaging(@RequestParam Integer start, @RequestParam Integer size) {
        List<Restaurant> listRestaurant = restaurantService.listAll();
        if(start >= 0 && size > 0 && start+size < listRestaurant.size()){
            listRestaurant = listRestaurant.subList(start, start+size);
        }
        else
        {
            return null;
        }
        return listRestaurant;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Restaurant getRestaurant(@PathVariable Integer id) {
        System.out.println(id);
        return restaurantService.getById(id);
    }

    // --- POST/PUT OPERATIONS ---
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = {"/", "", "/{id}"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public void addOrUpdateRestaurant(@RequestBody Restaurant restaurant) {
        restaurantService.saveOrUpdate(restaurant);
    }

    // --- DELETE OPERATIONS ---
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRestaurant(@PathVariable Integer id) {
        restaurantService.delete(id);
    }
}
