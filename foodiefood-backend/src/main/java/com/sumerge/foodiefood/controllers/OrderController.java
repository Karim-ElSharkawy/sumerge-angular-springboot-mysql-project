package com.sumerge.foodiefood.controllers;

import com.sumerge.foodiefood.model.Order;
import com.sumerge.foodiefood.model.User;
import com.sumerge.foodiefood.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users/{userId}/orders")
public class OrderController {
    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    // --- GET OPERATIONS ---
    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public List<Order> listOrders(@PathVariable Integer userId) {
        return orderService.listAllByUserId(userId);
    }

    @RequestMapping(value = "/{orderId}", method = RequestMethod.GET)
    public Order getItem(@PathVariable Integer userId, @PathVariable Integer orderId) {
        return orderService.getById(orderId);
    }

    // --- POST/PUT OPERATIONS ---
    @RequestMapping(value = {"/", "", "/{orderId}"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public Integer addOrUpdateItem(@RequestBody Order order, @PathVariable Integer userId) {
        User user = new User();
        user.setId(userId);
        order.setUser(user);
        orderService.saveOrUpdate(order);
        return order.getId();
    }

    // --- DELETE OPERATIONS ---
    @RequestMapping(value = "/{orderId}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer userId, @PathVariable Integer orderId) {
        orderService.delete(orderId);
    }
}
