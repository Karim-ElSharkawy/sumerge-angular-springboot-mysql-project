package com.sumerge.foodiefood.controllers;

import com.sumerge.foodiefood.model.User;
import com.sumerge.foodiefood.services.UserService;
import com.sumerge.foodiefood.util.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public List<User> listUsers() {
        return userService.listAll();
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public User getUser(@PathVariable Integer userId) {
        return userService.getById(userId);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Integer getUserByUsername(@RequestParam String username) {
        return userService.findUserExistsByName(username);
    }
    // --- POST/PUT OPERATIONS ---
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public Integer postUserLoginByCred(@RequestParam String username, @RequestParam String password) {
        return userService.loginUserByCred(username, new SecurityConfig().passwordEncoder().encode(password));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = {"/", "", "/{id}"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public Integer addOrUpdateUser(@RequestBody User user) {
        user.setPassword(new SecurityConfig().passwordEncoder().encode(user.getPassword()));
        userService.saveOrUpdate(user);
        return user.getId();
    }

    // --- DELETE OPERATIONS ---
    @RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable Integer userId) {
        userService.delete(userId);
    }
}
