package com.sumerge.foodiefood.controllers;

import com.sumerge.foodiefood.model.Order;
import com.sumerge.foodiefood.model.OrderItem;
import com.sumerge.foodiefood.services.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users/{userId}/orders/{orderId}/orderitems")
public class OrderItemController {
    private OrderItemService orderItemService;

    @Autowired
    public void setOrderItemService(OrderItemService orderItemService) {
        this.orderItemService = orderItemService;
    }

    // --- GET OPERATIONS ---
    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public List<OrderItem> getOrderItems(@PathVariable Integer userId, @PathVariable Integer orderId) {
        return orderItemService.listAllByOrderId(orderId);
    }

    // --- POST/PUT OPERATIONS ---
    @RequestMapping(value = {"/", ""}, method = {RequestMethod.POST, RequestMethod.PUT})
    public void addOrUpdateOrderItem(@RequestBody OrderItem orderItem, @PathVariable Integer userId, @PathVariable Integer orderId) {
        Order order = new Order();
        order.setId(orderId);
        orderItem.setOrder(order);
        orderItemService.saveOrUpdate(orderItem);
    }

    // --- DELETE OPERATIONS ---
    @RequestMapping(value = "/{orderItemId}", method = RequestMethod.DELETE)
    public void deleteOrderItem(@PathVariable Integer userId, @PathVariable Integer orderId, @PathVariable Integer orderItemId) {
        orderItemService.delete(orderItemId);
    }
}
