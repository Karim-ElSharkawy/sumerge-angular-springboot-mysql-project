package com.sumerge.foodiefood.controllers;

import com.sumerge.foodiefood.util.MyUserDetailsService;
import com.sumerge.foodiefood.model.AuthenticationRequest;
import com.sumerge.foodiefood.model.AuthenticationResponse;
import com.sumerge.foodiefood.util.JwtUtil;
import com.sumerge.foodiefood.util.MyUserPrincipal;
import com.sumerge.foodiefood.util.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("")
public class IndexController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        final MyUserPrincipal userDetails = (MyUserPrincipal) userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        try { // bugged
            if(new SecurityConfig().passwordEncoder().encode(authenticationRequest.getPassword()).equals(userDetails.getPassword())){
                throw new BadCredentialsException("Bad credentials");
            }
        }
        catch (BadCredentialsException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Incorrect username or password: "+ e.getMessage());
        }




        final String jwt = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

}
